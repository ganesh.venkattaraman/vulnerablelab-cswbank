<?php
    /* Avoid multiple sessions warning
    Check if session is set before starting a new one. */
    if(!isset($_SESSION)) {
        session_start();
    }

    include "validate_customer.php";
    include "connect.php";
    include "header.php";
    include "customer_navbar.php";
    include "customer_sidebar.php";
    include "session_timeout.php";


    $id = $_SESSION['loggedIn_cust_id'];
    $type =  $_POST["type"];
    $old_pwd =  $_POST["old_pwd"];
    $new_pwd =  $_POST["new_pwd"];
    $check_pwd =  $_POST["check_pwd"];




    $body = 'To Reset the password, please <a href="http://'.$_SERVER['HTTP_HOST'].'/pass_change_action.php?id = '.$id.'&type='.$type.'&old_pwd='.$old_pwd.'&new_pwd='.$new_pwd.'&check_pwd='.$check_pwd.'">click here</a>.';


?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="action_style.css">
</head>

<body>
    <div class="flex-container">
        <div class="flex-item">
        <p id="info"><?php echo $body;?></p>
        </div>

        <div class="flex-item">
            <a href="/customer_home.php" class="button">Home</a>
        </div>

    </div>

</body>
</html>