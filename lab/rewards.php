<?php
    /* Avoid multiple sessions warning
    Check if session is set before starting a new one. */
    if(!isset($_SESSION)) {
        session_start();
    }

    include "validate_customer.php";
    include "connect.php";
    include "header.php";
    include "customer_navbar.php";
    include "customer_sidebar.php";
    include "session_timeout.php";


?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="customer_add_style.css">
</head>

<body>
<form class="add_customer_form" action="rewprocess.php" method="get">
        <div class="flex-container-form_header">
            <h1 id="form_header">Purchase Rewards</h1>
        </div>

        <div class="flex-container" style = "position:relative; left:100px;">
        <div class="flex-item" >
            <label for="name">Name</label>
            <input id="name" name="name" type="text" class="text" value="" style = "position:relative; left:170px; top:-2px;"/>    
        </div>
        </div>

        <div class="flex-container" style = "position:relative; left:100px;">
            <div class="flex-item">
                <label for="email">Gift Card number</label>
                <input id="rew" name="rew" type="text" class="text" style = "position:relative; left:30px; top:2px;"/>
            </div>
        </div>


        <div class="flex-container" style = "position:relative; left:100px;">
            <div class="container">
                <button type="submit">Get Reward</button>
                <button type="reset" class="reset" onclick="return confirmReset();">Reset</button>
            </div>
        </div>

    </form>

        <div class="flex-item">
            <a href="/customer_home.php" class="button">Home</a>
        </div>

    </div>

</body>
</html>