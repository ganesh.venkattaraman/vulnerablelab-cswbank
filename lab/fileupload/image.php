<?php 
    /* Avoid multiple sessions warning
    Check if session is set before starting a new one. */
    if(!isset($_SESSION)) {
        session_start();
    }

    include "validate_customer.php";
    include "connect.php";
    include "header.php";
    include "customer_navbar.php";
    include "customer_sidebar.php";
    include "session_timeout.php";
	//Bypass using first extension
	//the developer is extracting the file extension by looking for the ‘.’ character in the filename, and extracting the string after the dot character.

	$status = "";

	$filename=$_FILES['fileToUpload']['name'];
	if($filename == ""){
		$status = "Come on! Please upload Some files to process";
			exit();
		}
	$file_array = explode(".", $filename);
	if(count($file_array) == 2 && $file_array[1] == "php" || $file_array[1] == "php") {
		$status = "PHP not allowed."; //Attempted attack detected
	}
	elseif(count($file_array) == 2 && $file_array[1] == "jpg" || $file_array[1] == "jpg"){
	$uploaddir = 'fileupload/uploads/';
	$uploadfile = $uploaddir . basename($_FILES['fileToUpload']['name']);
	if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $uploadfile)) {
		$status = "".$file_array[0]." is valid, and was successfully uploaded.\n";
	}
	}
	else{
		$status = "invalid file";
	}

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="action_style.css">
</head>

<body>
    <div class="flex-container">
        <div class="flex-item">
        <p id="info"><?php echo $status; ?></p>
        </div>

        <div class="flex-item">
            <a href="/customer_home.php" class="button">Home</a>
        </div>

    </div>

</body>
</html>
