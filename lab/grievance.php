<?php
    /* Avoid multiple sessions warning
    Check if session is set before starting a new one. */
    if(!isset($_SESSION)) {
        session_start();
    }

    include "validate_customer.php";
    include "connect.php";
    include "header.php";
    include "customer_navbar.php";
    include "customer_sidebar.php";
    include "session_timeout.php";


?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="customer_add_style.css">

</head>

<body>
<form class="add_customer_form" action="grievprocess.php" method="get">
        <div class="flex-container-form_header">
            <h1 id="form_header">Feedback</h1>
        </div>

        <div class="flex-container" style = "position:relative; left:100px;">
        <div class="flex-item" >
            <label for="name">Name</label>
            <input id="name" name="name" type="text" class="text" value="" style = "position:relative; left:170px; top:-2px;"/>    
        </div>
        </div>

        <div class="flex-container" style = "position:relative; left:100px;">
            <div class="flex-item">
                <label for="tel">Phone Number</label>
                <input id="tel" name="tel" type="text" class="text" value="" style = "position:relative; left:80px; top:2px;"/>
            </div>
        </div>

        <div class="flex-container" style = "position:relative; left:100px;">
            <div class="flex-item">
                <label for="email">Email</label>
                <input id="email" name="email" type="text" class="text" value="" style = "position:relative; left:175px; top:2px;"/>
            </div>
        </div>

        <div class="flex-container" style = "position:relative; left:100px;">
            <div class="flex-item">
                <label for="email">Grievance Form link</label>
                <input id="griev" name="griev" type="text" class="text" style = "position:relative; left:30px; top:2px;"/>
            </div>
        </div>


        <div class="flex-container" style = "position:relative; left:100px;">
            <div class="container">
                <button type="submit">Submit</button>
                <button type="reset" class="reset" onclick="return confirmReset();">Reset</button>
            </div>
        </div>

    </form>
    <div class="flex-container">
        <div class="container">
            <a href="/customer_home.php" class="button">Home</a>
        </div>
    </div>

</body>
</html>