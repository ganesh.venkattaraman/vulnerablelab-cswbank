<?php 
error_reporting(0);
    /* Avoid multiple sessions warning
    Check if session is set before starting a new one. */
    if(!isset($_SESSION)) {
        session_start();
    }

    include "validate_customer.php";
    include "connect.php";
    include "header.php";
    include "customer_navbar.php";
    include "customer_sidebar.php";
    include "session_timeout.php";
	//Bypass using first extension
	//the developer is extracting the file extension by looking for the ‘.’ character in the filename, and extracting the string after the dot character.

	$status = "";

	$imageinfo = getimagesize($_FILES['fileToUpload']['tmp_name']);	
	if($imageinfo['mime'] != 'image/png') 
		{
				//print image_type_to_mime_type($imageinfo[2]);
			$status = "Sorry, we only accept PNG files\n";
		}

		else {
			$uploaddir = 'fileupload/uploads/';
			$uploadfile = $uploaddir . basename($_FILES['fileToUpload']['name']);
			if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $uploadfile)) 
			{ $status =  "File is valid, and was successfully uploaded.\n"; } 

			else { $status =  "File uploading failed.\n"; }
		}


?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="action_style.css">
</head>

<body>
    <div class="flex-container">
        <div class="flex-item">
        <p id="info"><?php echo $status; ?></p>
        </div>

        <div class="flex-item">
            <a href="/kyc.php" class="button">Back</a>
        </div>

    </div>

</body>
</html>
