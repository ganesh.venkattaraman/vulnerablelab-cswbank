<?php
    /* Avoid multiple sessions warning
    Check if session is set before starting a new one. */
    if(!isset($_SESSION)) {
        session_start();
    }

    include "validate_customer.php";
    include "connect.php";
    include "header.php";
    include "customer_navbar.php";
    include "customer_sidebar.php";
    include "session_timeout.php";

    $id = $_SESSION['loggedIn_cust_id'];

    $sql0 = "SELECT * FROM customer WHERE cust_id=".$id;
    $sql1 = "SELECT * FROM passbook".$id." WHERE trans_id=(
                    SELECT MAX(trans_id) FROM passbook".$id.")";

    $result0 = $conn->query($sql0);
    $result1 = $conn->query($sql1);

    if ($result0->num_rows > 0) {
        // output data of each row
        while($row = $result0->fetch_assoc()) {
            $fname = $row["first_name"];
            $lname = $row["last_name"];
            $aadhar = $row["aadhar_no"];
            
        }
    }

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="customer_add_style.css">
</head>

<body>

        <div class="flex-container-form_header">
            <h1 id="form_header">Few steps away from completing your KYC !!!</h1>
        </div>

        <div class="flex-container">
            <div class=container>
                <label>First Name : <label id="info_label"><?php echo $fname ?></label></label>
            </div>
            <div class=container>
                <label>Last Name : <label id="info_label"><?php echo $lname ?></label></label>
            </div>
        </div>

        <div class="flex-container">
            <div class=container>
                <label>Upload Your PAN Card <label id="info_label"></label></label>
            </div>
            
        </div>

        <div class="flex-container">
            <div class=container>
                <form action="pan.php" method="post" enctype="multipart/form-data">
                    <input type="file" name="fileToUpload" id="fileToUpload" required>
			        <button type="submit">Upload</button>
                </form>
            </div>
        </div>

        <div class="flex-container">
            <div class=container>
                <label>Aadhar No : <label id="info_label"><?php echo $aadhar ?></label></label>
            </div>
        </div>

        <div class="flex-container">
            <div class=container>
                <label>Update Your Aadhar Card <label id="info_label"></label></label>
            </div>
            </form>
        </div>

        <div class="flex-container">
            <div class=container>
                <form action="aadhar.php" method="post" enctype="multipart/form-data">
                    <input type="file" name="fileToUpload" id="fileToUpload" required>
			        <button type="submit">Upload</button>
                </form>
            </div>
        </div>

        <div class="flex-container">
            <div class="container">
                <a href="/customer_home.php" class="button">Home</a>
            </div>
        </div>

    

</body>
</html>