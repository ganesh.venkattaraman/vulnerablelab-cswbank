## Built with
<b>HTML5, CSS, JavaScript</b> & <b>jQuery</b> used for front-end design.


<b>PHP7 & MySQL</b> used for back-end design.


<b>Oracle MySQL</b> has been used to create and host the database for the
internet banking website.


Other than the languages/tools mentioned above <b>no</b> other/external
libraries and/or web-page templates have been used, everything has been
coded from ground-up straight from scratch.

## How to build/use
Setup an environment which supports web development like <b>LAMP</b> on <b>Linux</b> systems OR install <b>WampServer/XAMPP</b> or anything similar on <b>Windows</b>.

Copy the folder lab or the files in it to the "/var/www/html" folder.

Import the bank.sql database into your MySQL setup.

Edit the file connect.php and give proper username and password of your MySQL setup.

Open a browser and test wether the setup works or not by visiting the home page. Type "localhost/home.php" as the URL in the browser to visit the home page. If you are using the ova file add the ip address to hosts file and map it to host cswlab.local.

All the passwords and the usernames of both the admin and the customer can be found in the sql database.

However some important usernames and passwords are provided below :
* Username of admin is "admin" & password is "password123".
* There is an SQL injection vulnerability in the customer login page. Just get in and see the password for yourself.

## Setting up the chatbot
1. Change host address in chat.php and server.php
2. Go to your shell command-line interface
3. type: 
	php -q c:\path\server.php
4. Navigate to the chat page. 
